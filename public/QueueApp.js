(function () {

	angular.module('QueueApp', [])
		.controller('QueueController', QueueController);

	QueueController.$inject = ['$scope', '$http'];

	/**
	* Bonus points - manipulating the without waiting for the
	* server request
	*/
	function QueueController($scope, $http) {
		$scope.products =  [
			{ name: 'Grammatical advice' },
			{ name: 'Magnifying glass repair' },
			{ name: 'Cryptography advice' }
		];
		$scope.customers = [];
		$scope.customersServed = [];

		_getProducts();
		_getCustomers();
		_getServedCustomers();

		$scope.addNewCustomer = function(){
			_addCustomer();
		}
		$scope.onCustomerAdded = function () {
			_getCustomers();
		};

		$scope.onCustomerRemoved = function () {
			_getCustomers();
		};

		$scope.onCustomerServed = function () {
			_getCustomers();
			_getServedCustomers();
		};

		$scope.serveCustomer = function(customer){
			_serveCustomer(customer);
		}

		$scope.removeCustomer = function(customer){
			_removeCustomer(customer);
		}

		function _getProducts(){
			return $scope.products;
		}

		function _getServedCustomers() {
			return $http.get('/api/customers/served').then(function (res) {
				$scope.customersServed = res.data;
			});
		}

		function _getCustomers() {
			return $http.get('/api/customers').then(function (res) {

				$scope.customers = res.data;
			});
		}

		function _addCustomer() {
			var data = {
				"name": $scope.newCustomerName,
				"product":{
					"name":$scope.newCustomerProduct
				},
				"joinedTime": new Date().toString()
			};
			return $http({
				method: 'POST',
				url:'/api/customer/add',
				data: data
			}).success(function(data,status,headers){
				document.getElementById('name').value = '';
				swal(data);
			});
		}

		function _serveCustomer(customer){
			var data = {
				"id":customer.id
			}
			return $http({
				method: 'PUT',
				url: '/api/customer/serve',
				data: data
			}).success(function(data,status,headers){
				swal(data);
			});
		}

		function _removeCustomer(customer) {

			var data = {
				"id":customer.id
			}
			return $http({
				method: 'DELETE',
				url: '/api/customer/remove?id='+customer.id,

			}).success(function(data,status,headers){
				swal(data);
			});
		}

	}

})();
